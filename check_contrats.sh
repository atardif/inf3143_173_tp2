#!/bin/bash
#
# Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

out=out
build_file=$out/contrats.build

mkdir -p out

make build-contrats -s > $build_file 2>&1

# Check empty output
if [ -s $build_file ]; then
	echo -ne "[\e[31mERR\e[0m] "
	echo -ne "\e[1mCOMPILING CONTRACTS\e[0m "
	echo -e "(cat $build_file)"
	exit 1
fi

# Run tests
for test_path in `ls Mages/test/contrats/*.java`; do
	test_file=`basename $test_path`
	test=${test_file%.*}
	res_file=$out/$test.out
	echo -ne " * "

	java -javaagent:Mages/lib/cofoja.asm-1.3-20160207.jar -cp bin/ contrats/$test > $res_file.tmp 2>&1
	cat $res_file.tmp | sed "s/Error:.*/Error\./" | sed "/\t.*/d" > $res_file

	diff sav/$test.res $res_file > $out/$test.diff
	if [ -s $out/$test.diff ]; then
		echo -ne "[\e[31mKO\e[0m] "
		echo -ne "\e[1m$test\e[0m "
		echo -e "(diff sav/$test.res $res_file)"
	else
		echo -ne "[\e[32mOK\e[0m] "
		echo -e "\e[1m$test\e[0m"
	fi
done
